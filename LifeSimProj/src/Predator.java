import java.util.UUID;
import java.util.ArrayList;
import java.util.Arrays;

public class Predator extends GameObject {
	private String aggressionGene, strengthGene, speedGene;
	private int  MAX_OFFSPRING,
		timeMoved=1, energyLevel, moveTime = 0, afterHunt = 10*60;
	private double MAINTAIN_SPEED, MAX_SPEED, postRepTime, postBirthTime=0, ENERGY_OUTPUT, ENERGY_TO_REPRODUCE;
	private double GESTATION;
	private boolean hunting, mating;
	private UUID uuid = UUID.randomUUID();
	private GameObject prey;
	private Predator mate;
	int[] randLoc = new int[] {0,0};
	String mateStrength = "",mateAggression = "", mateSpeed = "";
	GameObjectManager y=GameObjectManager.getInstance();
	/*
	 * Parameters:
	 * int X_POS: X Position of predator
	 * int Y_POS: Y Position of predator
	 * String aggressionG: Aggression Gene
	 * String strengthG: Strength Gene
	 * String speedG: Speed Gene
	 * int e2R: ENERGY_TO_REPRODUCE, aka energy predator needs to reproduce
	 * int eOutput: ENERGY_OUPUT
	 * int speedTime: MAINTAIN_SPEED
	 * int MAX_SPEED_HOD: max speed of predators with FF speed gene
	 * int MAX_SPEED_HED: max speed of predators with Ff speed gene
	 * int MAX_SPEED_HOR: max speed of predators with ff speed gene
	 * int gest: GESTATION
	 * int energy: initial energy level. Determined by xml file for new births
	 */
	public Predator(int X_POS, int Y_POS, String aggressionG, String strengthG, String speedG, double e2R, double eOutput, double speedTime, double MAX_SPEED_HOD, double MAX_SPEED_HED, double MAX_SPEED_HOR, double gest, int energy) {
		aggressionGene = aggressionG;
		strengthGene = strengthG;
		speedGene = speedG;
		ENERGY_TO_REPRODUCE = e2R;
		ENERGY_OUTPUT = eOutput;
		MAINTAIN_SPEED = speedTime;
		if (speedGene.equals("FF")) {
			MAX_SPEED = MAX_SPEED_HOD;
		} else if (speedGene.equals("Ff")) {
			MAX_SPEED = MAX_SPEED_HED;
		} else {
			MAX_SPEED = MAX_SPEED_HOR;
		}
		hunting = false;
		mating = false;
		postBirthTime = 3700;
		GESTATION = gest;
		postRepTime = 0;
		this.X_POS = X_POS;
		this.Y_POS = Y_POS;
		energyLevel = energy;
	}
	public void setBirthTime() {
		postBirthTime = 1;
	}
	/* 
	 * returns true if it's ready to birth new predators. Call getMate() to get the genes of the predator it reproduced with
	 * For 10 minutes after mating, move around randomly.
	 * Go straight into mating or hunting if previously on that
	 * Scan for new location otherwise, and then move into mating and hunting.
	 * Delete if energy <= 0;
	 */
	public boolean Run() {
		//int[] loc = new int[] {500,500};
		//Travel(loc);
		boolean rep = false;
		/*if(postBirthTime == 3600) {
			postBirthTime = 0;
		}*/
		if (postRepTime >= Math.round(GESTATION*24*60*60)) {
			rep = true;
			postRepTime=0;
		}
		if(postBirthTime > 0) {
			postBirthTime++;
		}
		/*if (postBirthTime <= 3600 && postBirthTime > 0) { //supposed to be an hour but that's too long
			int[] loc = new int[] {0,0};
			moveTime++;
			if (moveTime <60) {
				loc = randLoc;
			} else {
				loc[0] = (int)(Math.random()*y.getInstance().getMaxX());
				loc[1] = (int)(Math.random()*y.getInstance().getMaxY());
				moveTime = 0;
			}
			postBirthTime++;
			Travel(loc);
		} else*/
		if(postRepTime < Math.round(GESTATION*24*60*60) && postRepTime > 0) {
			postRepTime++;
		}
		/*else if (afterHunt < 600) {
			int[] loc = new int[] {0,0};
			moveTime++;
			if (moveTime <60) {
				loc = randLoc;
			} else {
				loc[0] = (int)(Math.random()*y.getInstance().getMaxX());
				loc[1] = (int)(Math.random()*y.getInstance().getMaxY());
				moveTime = 0;
			}
			afterHunt++;
			Travel(loc);
		}*/ /*if (hunting) {
			Hunt(prey);
		} else if(mating) {
			Reproduce(mate);
		}else {*/
			int[] loc = Scan2(CanReproduce());
			int[] noloc = new int[] {-1,-1};
			if (Arrays.equals(loc, noloc)) {
				moveTime++;
				if (moveTime <60) {
					loc = randLoc;
				} else {
					loc[0] = (int)(Math.random()*y.getInstance().getMaxX());
					loc[1] = (int)(Math.random()*y.getInstance().getMaxY());
					moveTime = 0;
				}
				//loc[0] = (int)(Math.random()*y.getInstance().getMaxX());
				//loc[1] = (int)(Math.random()*y.getInstance().getMaxY());
			}
			if (hunting) {
				Hunt(prey);
			} else if(mating) {
				Reproduce(mate);
			} else {
				Travel(loc);
			}
		//}
		/*if (rep) {
			postBirthTime = 0;
		}*/
		if(energyLevel <= 0) {
			for(int i=0; i < y.getInstance().getPredators().size(); i++)
			{
				if (getUUID().compareTo(y.getInstance().getPredators().get(i).getUUID())==0)
				{
					y.getInstance().getPredators().remove(i);
					break;
				}
			}
		}
		return rep;
	}
	/*
	 * Returns predators energy
	 */
	public int getEnergy() {
		return energyLevel;
	}
	/*
	 * sets energy level to e
	 */
	public void setEnergy(int e) {
		energyLevel = e;
	}
	/*
	 * Returns the strength gene of the predator
	 */
	public String getStrength() {
		return strengthGene;
	}
	/*
	 * Returns the aggression gene of the predator
	 */
	public String getAggression() {
		return aggressionGene;
	}
	/*
	 * Returns the speed gene of the predator
	 */
	public String getSpeed() {
		return speedGene;
	}
	/*
	 * Returns true if predator is currently going towards a potential mate
	 */
	public boolean isMating() {
		return mating;
	}
	/*
	 * Returns the genes of the predator's mate
	 */
	public String[] getMate() {
		String[] genes = new String[] {mateStrength,mateAggression,mateSpeed};
		return genes;
	}
	/*
	 * Returns true if the predator is currently chasing something to eat
	 */
	public boolean isHunting() {
		return hunting;
	}
	/*
	 * Improved scan method.
	 * If the predator can reproduce, it looks for a mate
	 * if none are found, if it has a nonaggressive gene it looks for predators to run away from
	 * if none are found, if it is completely agressive, it looks for predators to hunt
	 * if none are found, it looks for grazers to hunt
	 * if none are found, if it had middling agression, it looks for predators to hunt
	 * Returns location of where it's trying to go
	 */
	private int[] Scan2(boolean r) {
		int[] loc = new int[] {-1,-1};
		int[] noloc = new int[] {-1,-1};
		ArrayList<Grazer> gList = y.getInstance().getGrazers();
		ArrayList<GameObject> blocks = new ArrayList<GameObject>();
		ArrayList<Obstacle> oList = y.getInstance().getObstacles();
		ArrayList<Plant> plList = y.getInstance().getPlants();
		ArrayList<Predator> pList = y.getInstance().getPredators();
		for (Obstacle o : oList) {
			double distance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(o);
			}
		}
		/*for (Plant pl : plList) {
			double distance = Math.sqrt(Math.pow(pl.GetXPos()-X_POS,2)+Math.pow(pl.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(pl);
			}
		}*/
		double minDistance = 10000;
		if (r && (postRepTime >= Math.round(GESTATION*60*60*24) || postRepTime < 1)) {
			for (Predator g: pList) {
				double distance = Math.hypot(g.GetXPos()-this.GetXPos(), g.GetYPos()-this.GetYPos());
				if (g.getUUID() != uuid && (g.isMating() || g.CanReproduce())) {
					if (distance < 25 && distance < minDistance) {
						loc[0] = g.GetXPos();
						loc[1] = g.GetYPos();
						mate = g;
						mating = true;
						minDistance = distance;
					} else if (distance < 150 && distance < minDistance) {
						boolean blocked = false;
						for (GameObject o: blocks) {
							double oRadius = 0;
							if (o instanceof Plant) {
								oRadius = ((Plant)o).getDiameter()/2;
							} else if (o instanceof Obstacle) {
								oRadius = ((Obstacle)o).getDiameter()/2;
							}
							double baX = g.GetXPos() - X_POS;
							double baY = g.GetYPos() - Y_POS;
							double caX = o.GetXPos() - X_POS;
							double caY = o.GetYPos() - Y_POS;
							double a = baX * baX + baY * baY;
							double bBy2 = baX * caX + baY * caY;
							double c = caX * caX + caY * caY - oRadius*oRadius;
							double pBy2 = bBy2 / a;
							double q = c/a;
							double blockDistance = Math.hypot(o.GetXPos()-X_POS,o.GetYPos()-Y_POS);
							if (pBy2 * pBy2 - q < 0 && blockDistance <= distance) {
								blocked = true;
							}
						}
						if (!blocked) {
							loc[0] = g.GetXPos();
							loc[1] = g.GetYPos();
							minDistance = distance;
							mate = g;
							mating = true;
						}
					}
				}
			}
		}
		if (aggressionGene.equals("aa")) {
			for (Predator p : pList) {
				double distance = Math.hypot(p.GetXPos()-X_POS, p.GetYPos()-Y_POS);
				if (p.getUUID() != uuid) {
					if (distance < 25 && distance < minDistance && p.isHunting()) {
						//loc[0] = (int)Math.round(X_POS-5*(p.GetXPos()-X_POS));
						//loc[1] = (int)Math.round(Y_POS-5*(p.GetYPos()-Y_POS));
						int xRun = (int)Math.round(X_POS-10*(p.GetXPos()-X_POS));
						int yRun = (int)Math.round(Y_POS-10*(p.GetYPos()-Y_POS));
						double travelAngle = Math.atan((double)(yRun-Y_POS)/(double)(xRun-X_POS));
						if (xRun < X_POS) {
							loc[0] = (int)Math.round(X_POS - 5*Math.cos(travelAngle));
							loc[1] = (int)Math.round(Y_POS - 5*Math.sin(travelAngle));
						} else {
							loc[0] = (int)Math.round(X_POS + 5*Math.cos(travelAngle));
							loc[1] = (int)Math.round(Y_POS + 5*Math.sin(travelAngle));
						}
						minDistance = distance;
					} else if (distance < 150 && distance < minDistance && p.isHunting()) {
						boolean blocked = false;
						for (GameObject o: blocks) {
							double oRadius = 0;
							if (o instanceof Plant) {
								oRadius = ((Plant)o).getDiameter()/2;
							} else if (o instanceof Obstacle) {
								oRadius = ((Obstacle)o).getDiameter()/2;
							}
							double baX = p.GetXPos() - X_POS;
							double baY = p.GetYPos() - Y_POS;
							double caX = o.GetXPos() - X_POS;
							double caY = o.GetYPos() - Y_POS;
							double a = baX * baX + baY * baY;
							double bBy2 = baX * caX + baY * caY;
							double c = caX * caX + caY * caY - oRadius*oRadius;
							double pBy2 = bBy2 / a;
							double q = c/a;
							double blockDistance = Math.hypot(o.GetXPos()-X_POS,o.GetYPos()-Y_POS);
							if (pBy2 * pBy2 - q < 0 && blockDistance <= distance) {
								blocked = true;
							}
						}
						if (!blocked) {
							int xRun = (int)Math.round(X_POS-10*(p.GetXPos()-X_POS));
							int yRun = (int)Math.round(Y_POS-10*(p.GetYPos()-Y_POS));
							double travelAngle = Math.atan((double)(yRun-Y_POS)/(double)(xRun-X_POS));
							if (xRun < X_POS) {
								loc[0] = (int)Math.round(X_POS - 5*Math.cos(travelAngle));
								loc[1] = (int)Math.round(Y_POS - 5*Math.sin(travelAngle));
							} else {
								loc[0] = (int)Math.round(X_POS + 5*Math.cos(travelAngle));
								loc[1] = (int)Math.round(Y_POS + 5*Math.sin(travelAngle));
							}
							minDistance = distance;
						}
					}
				}
			}
		}
		minDistance = 10000;
		if (aggressionGene.equals("AA") && postBirthTime > 3600 && Arrays.equals(loc, noloc) && (postRepTime > 600 || postRepTime < 1)) {
			for (Predator g: pList) {
				double distance = Math.hypot(g.GetXPos()-this.GetXPos(), g.GetYPos()-this.GetYPos());
				if (g.getUUID() != uuid) {
					if (distance < 25 && distance < minDistance) {
						loc[0] = g.GetXPos();
						loc[1] = g.GetYPos();
						prey = g;
						hunting = true;
						minDistance = distance;
					} else if (distance < 150 && distance < minDistance) {
						boolean blocked = false;
						for (GameObject o: blocks) {
							double oRadius = 0;
							if (o instanceof Plant) {
								oRadius = ((Plant)o).getDiameter()/2;
							} else if (o instanceof Obstacle) {
								oRadius = ((Obstacle)o).getDiameter()/2;
							}
							double baX = g.GetXPos() - X_POS;
							double baY = g.GetYPos() - Y_POS;
							double caX = o.GetXPos() - X_POS;
							double caY = o.GetYPos() - Y_POS;
							double a = baX * baX + baY * baY;
							double bBy2 = baX * caX + baY * caY;
							double c = caX * caX + caY * caY - oRadius*oRadius;
							double pBy2 = bBy2 / a;
							double q = c/a;
							double blockDistance = Math.hypot(o.GetXPos()-X_POS,o.GetYPos()-Y_POS);
							if (pBy2 * pBy2 - q < 0 && blockDistance <= distance) {
								blocked = true;
							}
						}
						if (!blocked) {
							loc[0] = g.GetXPos();
							loc[1] = g.GetYPos();
							minDistance = distance;
							prey = g;
							hunting = true;
						}
					}
				}
			}
		}
		if (Arrays.equals(loc, noloc)) {
			for (Grazer g: gList) {
				double distance = Math.hypot(g.GetXPos()-this.GetXPos(), g.GetYPos()-this.GetYPos());
				if (distance < 25 && distance < minDistance) {
					loc[0] = g.GetXPos();
					loc[1] = g.GetYPos();
					minDistance = distance;
					prey = g;
					hunting = true;
				} else if(distance < 150 && distance < minDistance ) {
					boolean blocked = false;
					for (GameObject o: blocks) {
						double oRadius = 0;
						if (o instanceof Plant) {
							oRadius = ((Plant)o).getDiameter()/2;
						} else if (o instanceof Obstacle) {
							oRadius = ((Obstacle)o).getDiameter()/2;
						}
						double baX = g.GetXPos() - X_POS;
						double baY = g.GetYPos() - Y_POS;
						double caX = o.GetXPos() - X_POS;
						double caY = o.GetYPos() - Y_POS;
						double a = baX * baX + baY * baY;
						double bBy2 = baX * caX + baY * caY;
						double c = caX * caX + caY * caY - oRadius*oRadius;
						double pBy2 = bBy2 / a;
						double q = c/a;
						double blockDistance = Math.hypot(o.GetXPos()-X_POS,o.GetYPos()-Y_POS);
						if (pBy2 * pBy2 - q < 0 && blockDistance <= distance) {
							blocked = true;
						}
					}
					if (!blocked) {
						loc[0] = g.GetXPos();
						loc[1] = g.GetYPos();
						minDistance = distance;
						prey = g;
						hunting = true;
					}
				}
			}
		}

		minDistance = 10000;
		if (Arrays.equals(loc,noloc) && aggressionGene.equals("Aa") && postBirthTime > 3600 && (postRepTime > 600 || postRepTime < 1)) {
			for (Predator g: pList) {
				double distance = Math.hypot(g.GetXPos()-this.GetXPos(), g.GetYPos()-this.GetYPos());
				if (g.getUUID() != uuid) {
					if (distance < 25 && distance < minDistance) {
						loc[0] = g.GetXPos();
						loc[1] = g.GetYPos();
						prey = g;
						hunting = true;
						minDistance = distance;
					} else if (distance < 150 && distance < minDistance) {
						boolean blocked = false;
						for (GameObject o: blocks) {
							double oRadius = 0;
							if (o instanceof Plant) {
								oRadius = ((Plant)o).getDiameter()/2;
							} else if (o instanceof Obstacle) {
								oRadius = ((Obstacle)o).getDiameter()/2;
							}
							double baX = g.GetXPos() - X_POS;
							double baY = g.GetYPos() - Y_POS;
							double caX = o.GetXPos() - X_POS;
							double caY = o.GetYPos() - Y_POS;
							double a = baX * baX + baY * baY;
							double bBy2 = baX * caX + baY * caY;
							double c = caX * caX + caY * caY - oRadius*oRadius;
							double pBy2 = bBy2 / a;
							double q = c/a;
							double blockDistance = Math.hypot(o.GetXPos()-X_POS,o.GetYPos()-Y_POS);
							if (pBy2 * pBy2 - q < 0 && blockDistance <= distance) {
								blocked = true;
							}
						}
						if (!blocked) {
							loc[0] = g.GetXPos();
							loc[1] = g.GetYPos();
							minDistance = distance;
							prey = g;
							hunting = true;
						}
					}
				}
			}
		}
		return loc;
	}
	/*private int[] Scan(boolean r) {
		int[] loc = new int[] {-1,-1};
		int[] noloc = new int[] {-1,-1};
		ArrayList<GameObject> blocks = new ArrayList<GameObject>();
		ArrayList<Obstacle> oList = y.getInstance().getObstacles();
		ArrayList<Plant> plList = y.getInstance().getPlants();
		for (Obstacle o : oList) {
			double distance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(o);
			}
		}
		for (Plant pl : plList) {
			double distance = Math.sqrt(Math.pow(pl.GetXPos()-X_POS,2)+Math.pow(pl.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(pl);
			}
		}
		if (aggressionGene.equals("aa")) {
			ArrayList<Predator> pList = y.getInstance().getPredators();
			double minDistance = 10000;
			for (Predator p : pList) {
				double distance = Math.sqrt(Math.pow(p.GetXPos()-X_POS,2)+Math.pow(p.GetYPos()-X_POS,2));
				if ( distance < minDistance && distance < 25 && p.isHunting()) {
						loc[0] = (int)Math.round(X_POS-10*(p.GetXPos()-X_POS));
						loc[1] = (int)Math.round(Y_POS-10*(p.GetYPos()-Y_POS));
						minDistance = distance;
				}
			}
		}
		if (Arrays.equals(loc, noloc)) {
			if (r) {
				ArrayList<Predator> pList = y.getInstance().getPredators();
				double minDistance = 10000;
				for (Predator p : pList) {
					double distance = Math.sqrt(Math.pow(p.GetXPos()-X_POS,2)+Math.pow(p.GetYPos()-X_POS,2));
					if (distance < minDistance && (p.CanReproduce() || p.isMating())) {
						if (distance < 25) {
								loc[0] = p.GetXPos();
								loc[1] = p.GetYPos();
								minDistance = distance;
								mate = p;
								mating = true;
						} else if (distance < 150) {
							boolean blocked = false;
							for (GameObject o: blocks) {
								double oRadius = 0;
								if (o instanceof Plant) {
									oRadius = ((Plant)o).getDiameter()/2;
								} else if (o instanceof Obstacle) {
									oRadius = ((Obstacle)o).getDiameter()/2;
								}
								double baX = p.GetXPos() - X_POS;
								double baY = p.GetYPos() - Y_POS;
								double caX = o.GetXPos() - X_POS;
								double caY = o.GetYPos() - Y_POS;
								double a = baX * baX + baY * baY;
								double bBy2 = baX * caX + baY * caY;
								double c = caX * caX + caY * caY - oRadius*oRadius;
								double pBy2 = bBy2 / a;
								double q = c/a;
								double blockDistance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
								if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
									blocked = true;
								}
							}
							if (!blocked) {
								loc[0] = p.GetXPos();
								loc[1] = p.GetYPos();
								minDistance = distance;
								mate = p;
								mating = true;
							}
						}
					}
				}
				/*if (!Arrays.equals(loc, noloc)) {
					Reproduce(mate);
				}
			} else {
				///search for food
				if (aggressionGene.equals("AA") && postBirthTime > 3600) {
					//search for grazers and predators to eat
					ArrayList<Predator> pList = y.getInstance().getPredators();
					double minDistance = 10000;
					for (Predator p : pList) {
						double distance = Math.sqrt(Math.pow(p.GetXPos()-X_POS,2)+Math.pow(p.GetYPos()-X_POS,2));
						if (distance < minDistance) {
							if (distance < 25) {
								loc[0] = p.GetXPos();
								loc[1] = p.GetYPos();
								minDistance = distance;
								prey = p;
								hunting = true;
							} else if (distance < 150) {
								boolean blocked = false;
								for (GameObject o: blocks) {
									double oRadius = 0;
									if (o instanceof Plant) {
										oRadius = ((Plant)o).getDiameter()/2;
									} else if (o instanceof Obstacle) {
										oRadius = ((Obstacle)o).getDiameter()/2;
									}
									double baX = p.GetXPos() - X_POS;
									double baY = p.GetYPos() - Y_POS;
									double caX = o.GetXPos() - X_POS;
									double caY = o.GetYPos() - Y_POS;
									double a = baX * baX + baY * baY;
									double bBy2 = baX * caX + baY * caY;
									double c = caX * caX + caY * caY - oRadius*oRadius;
									double pBy2 = bBy2 / a;
									double q = c/a;
									double blockDistance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
									if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
										blocked = true;
									}
								}
								
								if (!blocked) {
									loc[0] = p.GetXPos();
									loc[1] = p.GetYPos();
									minDistance = distance;
									prey = p;
									hunting = true;
								}
							}
							
						}
					}
					/*if (!Arrays.equals(loc, noloc)) {
						Hunt(prey);
					}
				} else {
					// search for grazers to eat
					ArrayList<Grazer> gList = y.getInstance().getGrazers();
					double minDistance = 10000;
					for (Grazer g : gList) {
						double distance = Math.sqrt(Math.pow(g.GetXPos()-X_POS,2)+Math.pow(g.GetYPos()-X_POS,2));
						if (distance < minDistance) {
							if (distance < 25) {
								loc[0] = g.GetXPos();
								loc[1] = g.GetYPos();
								minDistance = distance;
								prey = g;
								hunting = true;
							} else if (distance < 150) {
								boolean blocked = false;
								for (GameObject o: blocks) {
									double oRadius = 0;
									if (o instanceof Plant) {
										oRadius = ((Plant)o).getDiameter()/2;
									} else if (o instanceof Obstacle) {
										oRadius = ((Obstacle)o).getDiameter()/2;
									}
									double baX = g.GetXPos() - X_POS;
									double baY = g.GetYPos() - Y_POS;
									double caX = o.GetXPos() - X_POS;
									double caY = o.GetYPos() - Y_POS;
									double a = baX * baX + baY * baY;
									double bBy2 = baX * caX + baY * caY;
									double c = caX * caX + caY * caY - oRadius*oRadius;
									double pBy2 = bBy2 / a;
									double q = c/a;
									double blockDistance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
									if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
										blocked = true;
									}
								}
								if (!blocked) {
									loc[0] = g.GetXPos();
									loc[1] = g.GetYPos();
									minDistance = distance;
									prey = g;
									hunting = true;
								}
							}
						}
					}
					if (aggressionGene.equals("Aa") && !hunting) {
						//search for predators to eat
						ArrayList<Predator> pList = y.getInstance().getPredators();
						minDistance = 10000;
						for (Predator p : pList) {
							double distance = Math.sqrt(Math.pow(p.GetXPos()-X_POS,2)+Math.pow(p.GetYPos()-X_POS,2));
							if (distance < minDistance) {
								if (distance < 25) {
									loc[0] = p.GetXPos();
									loc[1] = p.GetYPos();
									minDistance = distance;
									prey = p;
									hunting = true;
								} else if (distance < 150) {
									boolean blocked = false;
									for (GameObject o: blocks) {
										double oRadius = 0;
										if (o instanceof Plant) {
											oRadius = ((Plant)o).getDiameter()/2;
										} else if (o instanceof Obstacle) {
											oRadius = ((Obstacle)o).getDiameter()/2;
										}
										double baX = p.GetXPos() - X_POS;
										double baY = p.GetYPos() - Y_POS;
										double caX = o.GetXPos() - X_POS;
										double caY = o.GetYPos() - Y_POS;
										double a = baX * baX + baY * baY;
										double bBy2 = baX * caX + baY * caY;
										double c = caX * caX + caY * caY - oRadius*oRadius;
										double pBy2 = bBy2 / a;
										double q = c/a;
										double blockDistance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
										if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
											blocked = true;
										}
									}
									if (!blocked) {
										loc[0] = p.GetXPos();
										loc[1] = p.GetYPos();
										minDistance = distance;
										prey = p;
										hunting = true;
									}
								}
							}
						}
					}
					/*if (!Arrays.equals(loc, noloc)) {
						Hunt(prey);
					}
				}
			}
		}
		return loc;
	}*/
	/*
	 * moves predator towards location given
	 * Checks for edge of map and obstacles to avoid
	 * decreases speed if it's run for a certain distance
	 * decreases energy per DU moved
	 */
	private void Travel(int[] loc) {
		if (y.getInstance().getMaxX() < loc[0] || loc[0] < 0 ) {
			loc[0] = (int)Math.round(X_POS);
		}
		if (y.getInstance().getMaxY() < loc[1] || loc[1] < 0) {
			loc[1] = (int)Math.round(Y_POS);
		}
		if(y.getInstance().getMaxX() == (int)Math.round(X_POS) || (int)Math.round(X_POS) == 0 )
		{
			loc[0] = (int)(Math.random()*y.getInstance().getMaxX());
		}

		if(y.getInstance().getMaxY() == (int)Math.round(Y_POS) || (int)Math.round(Y_POS) == 0 )
		{
			loc[1] = (int)(Math.random()*y.getInstance().getMaxY());
		}
		double travelAngle;
		if(loc[1] == (int)Math.round(Y_POS) && loc[0] == (int)Math.round(X_POS)) {
			return;
		}
		/*if(loc[1] == (int)Math.round(Y_POS)) {
			travelAngle = 0;
		} else if (loc[0] == (int)Math.round(X_POS)) {
			travelAngle = Math.PI/2;
		} else {*/
			travelAngle = Math.atan((double)(loc[1]-Y_POS)/(double)(loc[0]-X_POS));
		//}
		double speed = MAX_SPEED/60;
		if (timeMoved >= (MAINTAIN_SPEED*60)) {
			speed -= (Math.floor((timeMoved-(MAINTAIN_SPEED*60))/15))/60;
			if (speed <= 0) {
				timeMoved = 0;
				hunting = false;
				//prey = null;
				return;
			}
		}
		double oldx;
		double oldy;
		double newX;
		double newY;
		
		/*ArrayList<Obstacle> oList = y.getInstance().getObstacles();
		for( Obstacle o : oList) {
			while((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				travelAngle = (Math.random()*Math.PI/2)-Math.PI/4;
				newX = (int)(X_POS+speed*Math.cos(travelAngle));
				newY = (int)(Y_POS+speed*Math.sin(travelAngle));
			}
		}
		ArrayList<Plant> pList = y.getInstance().getPlants();
		for( Plant p : pList) {
			while((newX <= p.GetXPos() + (p.getDiameter()/2)) && (newX >= p.GetXPos() - (p.getDiameter()/2)) 
					&& (newY <= p.GetYPos() + (p.getDiameter()/2)) && (newY >= p.GetYPos() - (p.getDiameter()/2))) {
				travelAngle = (Math.random()*Math.PI/2)-Math.PI/4;
				newX = (int)(X_POS+speed*Math.cos(travelAngle));
				newY = (int)(Y_POS+speed*Math.sin(travelAngle));
			}
		}*/
		if (loc[0] < X_POS) {
			newX = X_POS - speed*Math.cos(travelAngle);
			newY = Y_POS - speed*Math.sin(travelAngle);
		} else {
			newX = X_POS + speed*Math.cos(travelAngle);
			newY = Y_POS + speed*Math.sin(travelAngle);
		}
		ArrayList<Obstacle> oList = y.getInstance().getObstacles();
		boolean block1 = false;
		boolean block2 = false;
		boolean block3 = false;
		for( Obstacle o : oList) {
			if ((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				block1 = true;
			}
			if ((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (Y_POS <= o.GetYPos() + (o.getDiameter()/2)) && (Y_POS >= o.GetYPos() - (o.getDiameter()/2))) {
				block2 = true;
			}
			if ((X_POS <= o.GetXPos() + (o.getDiameter()/2)) && (X_POS >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				block3 = true;
			}
		}
		if(block1) {
			if (block2) {
				if(block3) {
					if (loc[0] < X_POS) {
						newX = X_POS + speed*Math.cos(travelAngle);
						newY = Y_POS + speed*Math.sin(travelAngle);
					} else {
						newX = X_POS - speed*Math.cos(travelAngle);
						newY = Y_POS - speed*Math.sin(travelAngle);
					}
				} else {
					newX = X_POS;
					if(loc[1] < Y_POS) {
						newY = Y_POS - speed;
					} else {
						newY = Y_POS + speed;
					}
				}
			} else {
				newY = Y_POS;
				if(loc[0] < X_POS) {
					newX = X_POS - speed;
				} else {
					newX = X_POS + speed;
				}
			}
		}
		/*if (block1) {
			if(block2) {
				newX = X_POS;
				if(block3) {
					newY = Y_POS;
				}
			} else {
				newY = Y_POS;
			}
		}*/
		oldx=X_POS;
		if ((newX < loc[0] && X_POS > loc[0]) || (newX > loc[0] && X_POS < loc[0])) {
			//energyDrop = Math.sqrt(Math.pow(X_POS-loc[0],2)+Math.pow(Y_POS-loc[1],2));
			newX = loc[0];
		}
		if (!(newX > y.getInstance().getMaxX() || newX<0)) {
			X_POS = newX;
		}
		oldy=Y_POS;
		if ((newY < loc[1] && Y_POS > loc[1]) || (newY > loc[1] && Y_POS < loc[1])) {
			//energyDrop = Math.sqrt(Math.pow(X_POS-loc[0],2)+Math.pow(Y_POS-loc[1],2));
			newY = loc[1];
		}
		if (!(newY > y.getInstance().getMaxY() || newY<0)) {
			Y_POS = newY;
		}
		double energyDrop = ENERGY_OUTPUT*(Math.hypot(oldx-newX, oldy-newY))/5;
		//X_POS += speed*Math.cos(travelAngle);
		//Y_POS += speed*Math.sin(travelAngle);
		//energyLevel -= ENERGY_OUTPUT*speed/5;
		energyLevel -= energyDrop;
		timeMoved++;
		randLoc = loc;
	}
	/*
	 *  moves towards specific prey prespecified by Scan2()
	 *  if ontop of prey, eat it.
	 *  if prey is hidden, stop hunting it
	 */
	private void Hunt(GameObject g) {
		prey = g;
		ArrayList<GameObject> blocks = new ArrayList<GameObject>();
		ArrayList<Obstacle> oList = y.getInstance().getObstacles();
		ArrayList<Plant> plList = y.getInstance().getPlants();
		for (Obstacle o : oList) {
			double distance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(o);
			}
		}
		for (Plant pl : plList) {
			double distance = Math.sqrt(Math.pow(pl.GetXPos()-X_POS,2)+Math.pow(pl.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(pl);
			}
		}
		//double distance = Math.sqrt(Math.pow(g.GetXPos()-X_POS,2)+Math.pow(g.GetYPos()-Y_POS,2));
		double distance = Math.hypot(g.GetXPos()-this.GetXPos(), g.GetYPos()-this.GetYPos());
		if ((int)Math.round(X_POS) == g.GetXPos() && (int)Math.round(Y_POS) == g.GetYPos()) {
			if (g instanceof Grazer) {
				Eat((Grazer)g);
			} else if (g instanceof Predator) {
				Eat((Predator)g);
			}
			hunting = false;
			//prey = null;
		} else{
			if (distance < 25) {
				int[] preyLoc = new int[] {g.GetXPos(),g.GetYPos()};
				Travel(preyLoc);
			} else if (distance < 150) {
				boolean blocked = false;
				for (GameObject o: blocks) {
					double oRadius = 0;
					if (o instanceof Plant) {
						oRadius = ((Plant)o).getDiameter()/2;
					} else if (o instanceof Obstacle) {
						oRadius = ((Obstacle)o).getDiameter()/2;
					}
					double baX = g.GetXPos() - X_POS;
					double baY = g.GetYPos() - Y_POS;
					double caX = o.GetXPos() - X_POS;
					double caY = o.GetYPos() - Y_POS;
					double a = baX * baX + baY * baY;
					double bBy2 = baX * caX + baY * caY;
					double c = caX * caX + caY * caY - oRadius*oRadius;
					double pBy2 = bBy2 / a;
					double q = c/a;
					double blockDistance = Math.hypot(o.GetXPos()-(int)Math.round(X_POS),o.GetYPos()-(int)Math.round(Y_POS));
					if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
						blocked = true;
						hunting = false;
						//prey = null;
					}
				}
				if (!blocked) {
					int[] preyLoc = new int[] {g.GetXPos(),g.GetYPos()};
					Travel(preyLoc);
				}
			} else {
				hunting = false;
				//prey = null;
			}
		} /*else {
			hunting = false;
		}*/
	}
	/*
	 * moves towards potential made prespecified by Scan2()
	 * if ontop of mate, collect it's genes.
	 * if mate is hidden, stop going towards it
	 */
	private void Reproduce(Predator p) {
		ArrayList<GameObject> blocks = new ArrayList<GameObject>();
		ArrayList<Obstacle> oList = y.getInstance().getObstacles();
		ArrayList<Plant> plList = y.getInstance().getPlants();
		for (Obstacle o : oList) {
			double distance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(o);
			}
		}
		for (Plant pl : plList) {
			double distance = Math.sqrt(Math.pow(pl.GetXPos()-X_POS,2)+Math.pow(pl.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(pl);
			}
		}
		double distance = Math.sqrt(Math.pow(p.GetXPos()-X_POS,2)+Math.pow(p.GetYPos()-X_POS,2));
		/*if ((int)Math.round(X_POS) == p.GetXPos() && (int)Math.round(Y_POS) == p.GetYPos()) {
			postRepTime = 1;
			mating = false;
			mateAggression = p.getAggression();
			mateSpeed = p.getSpeed();
			mateStrength = p.getStrength();
		} else*/ if (p.CanReproduce() || p.isMating()) {
			if ((int)Math.round(X_POS) == p.GetXPos() && (int)Math.round(Y_POS) == p.GetYPos()) {
				postRepTime = 1;
				mating = false;
				mateAggression = p.getAggression();
				mateSpeed = p.getSpeed();
				mateStrength = p.getStrength();
			} else if (distance < 25) {
				int[] mateLoc = new int[] {p.GetXPos(),p.GetYPos()};
				Travel(mateLoc);
			} else if (distance < 150) {
				boolean blocked = false;
				for (GameObject o: blocks) {
					double oRadius = 0;
					if (o instanceof Plant) {
						oRadius = ((Plant)o).getDiameter()/2;
					} else if (o instanceof Obstacle) {
						oRadius = ((Obstacle)o).getDiameter()/2;
					}
					double baX = p.GetXPos() - X_POS;
					double baY = p.GetYPos() - Y_POS;
					double caX = o.GetXPos() - X_POS;
					double caY = o.GetYPos() - Y_POS;
					double a = baX * baX + baY * baY;
					double bBy2 = baX * caX + baY * caY;
					double c = caX * caX + caY * caY - oRadius*oRadius;
					double pBy2 = bBy2 / a;
					double q = c/a;
					double blockDistance = Math.hypot(o.GetXPos()-(int)Math.round(X_POS),o.GetYPos()-p.GetYPos());
					if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
						blocked = true;
						mating = false;
					}
				}
				if (!blocked) {
					int[] mateLoc = new int[] {p.GetXPos(),p.GetYPos()};
					Travel(mateLoc);
				}
			} else {
				mating = false;
			}
		} else {
			mating = false;
		}
	}
	
	/*private ArrayList<Predator> Birth(Predator p) {
		ArrayList<Predator> newCubs = new ArrayList<Predator>();
		int numCubs = (int)(Math.random()*MAX_OFFSPRING);
		for (int i = 0; i < numCubs; i++) {
			Predator p = new Predator(X_POS, Y_POS, aggressionG, strengthG, speedG, ENERGY_TO_REPRODUCE, int eOutput, int speedTime, int MAX_SPEED_HOD, int MAX_SPEED_HED, int MAX_SPEED_HOR);
			newCubs.add(p);
		}
	}*/

	/*
	 * attempt to eat grazer
	 * if success, get energy from it
	 * if failure, move on
	 */
	private void Eat(Grazer g) {
		double chanceKill = Math.random();
		boolean kill = (strengthGene.equals("SS") && chanceKill < .95) || 
				(strengthGene.equals("Ss") && chanceKill < .75) ||
				(strengthGene.equals("ss") && chanceKill < .5);
		if(kill) {
			energyLevel += .9*g.getEnergy();
			for(int i=0; i < y.getInstance().getGrazers().size(); i++)
			{
				if (g.getUUID().compareTo(y.getInstance().getGrazers().get(i).getUUID())==0)
				{
					y.getInstance().getGrazers().remove(i);
					break;
				}
			}
			//g.setEnergy(0);
		}
		hunting = false;
		afterHunt = 0;
	}
	/*
	 * attempt to eat predator
	 * if success, get energy from it
	 * if failure, move on
	 */
	private void Eat(Predator p) {
		double chanceKill = Math.random();
		boolean kill = false; // issue here
		String otherStrength = p.getStrength();
		if (strengthGene.equals(otherStrength)) {
			if (chanceKill < .25) {
				kill = true;
			} else if (chanceKill > .75) {
				int newE = (int) (p.getEnergy() + energyLevel*.9);
				p.setEnergy(newE);
				energyLevel = 0;
			}
		} else if (strengthGene.equals("SS")) {
			if (otherStrength.equals("Ss") && chanceKill <.75) {
				kill = true;
			} else if (otherStrength.equals("ss") && chanceKill < .95) {
				kill = true;
			}
		} else if (strengthGene.equals("Ss")) {
			if (otherStrength.equals("SS") && chanceKill < .25) {
				kill = true;
			} else if (otherStrength.equals("ss") && chanceKill < .75) {
				kill = true;
			}
		} else if (strengthGene.equals("ss")) {
			if (otherStrength.equals("SS") && chanceKill < .05) {
				kill = true;
			} else if (otherStrength.equals("Ss") && chanceKill < .25) {
				kill = true;
			}
		} 
		if (kill) {
			energyLevel += .9*p.getEnergy();
			for(int i=0; i < y.getInstance().getPredators().size(); i++)
			{
				if (p.getUUID().compareTo(y.getInstance().getPredators().get(i).getUUID())==0)
				{
					y.getInstance().getPredators().remove(i);
					break;
				}
			}
			//p.setEnergy(0);
		}
		hunting = false;
		afterHunt = 0;
	}
	/*
	 * Returns true if predator's energy level is high enough to start finding mate
	 */
	public boolean CanReproduce() {
		if (energyLevel >= ENERGY_TO_REPRODUCE) {
			return true;
		}
		return false;
	}
	/*
	 * returns predator UUID
	 */
	public UUID getUUID(){
		return uuid;
	}
}