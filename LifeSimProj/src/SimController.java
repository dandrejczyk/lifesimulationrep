
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class SimController {
	
	/*
	 * Variables
	 */
	SimView mainView;
	private FileChooser fileChooser = new FileChooser();
	private File selectedFile;
	
	/**
	 * Creates a new controller object
	 */
	public SimController(SimView view)
	{
		mainView = view;
		initView();
	}
	
	/**
	 * Initializes the view and adds event listeners to
	 * the components that need them
	 */
	private void initView()
	{
		
		/* 
	     * -------------------------
	     * | Create event handlers |
	     * -------------------------
	     */
		
		// Change speed
		EventHandler<ActionEvent> changeSpeed = new EventHandler<ActionEvent>()
	    {
			@Override
			public void handle(ActionEvent arg0) 
			{
				Driver.updateSpeed(mainView.getSpeedSelector().getValue());
			}
	    };
	    
	    // Upload file
	    EventHandler<ActionEvent> selectFile = new EventHandler<ActionEvent>()
	    {
	    	@Override
	    	public void handle(ActionEvent arg0) 
	    	{
	    		Stage currentStage = (Stage) mainView.getView().getWindow();
	    		selectedFile = fileChooser.showOpenDialog(currentStage);
	    		mainView.getFileUploadLabel().setText(selectedFile.getName());
	    		GameObjectManager.getInstance().setFile(selectedFile);
	    	}
	    };
	    
	    // Start simulation
	    EventHandler<ActionEvent> startSim = new EventHandler<ActionEvent>()
	    {
	    	@Override
	    	public void handle(ActionEvent arg0) 
	    	{
	    		Driver.startSim();
	    	}
	    };
	    
	    // Create Report
	    EventHandler<ActionEvent> createReport = new EventHandler<ActionEvent>()
	    {   	
	    	@Override
	    	public void handle(ActionEvent arg0) 
	    	{
	    		GameObjectManager.getInstance().createReport();
	    	}
	    };

	    // TO-DO: Add event handler/listener for creating report
	    
	    
	    
	    /* 
	     * -----------------------
	     * | Add event listeners |
	     * -----------------------
	     */
	    
	    // Add event listener to speed selector combo box
	    mainView.getSpeedSelector().setOnAction(changeSpeed);
	    
	    // Add event listener to Upload File Button
	    mainView.getFileUploadBtn().setOnAction(selectFile);
	    
	    // Add event listener to simulation start button
	    mainView.getSimStartBtn().setOnAction(startSim);
	    
	    // Add event listener to create report button
	    mainView.getReportBtn().setOnAction(createReport);
	}

}
