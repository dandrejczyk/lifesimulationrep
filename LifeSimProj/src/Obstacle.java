

public class Obstacle extends GameObject{
	private double diameter, height;
	Obstacle(int x, int y, double d, double h) {
		X_POS = x;
		Y_POS = y;
		diameter = d;
		height = h;
	}
	public double getDiameter() {
		return diameter;
	}
	public double getHeight() {
		return height;
	}
}
