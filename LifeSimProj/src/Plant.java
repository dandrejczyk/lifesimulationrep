

import java.util.UUID;
import java.util.*;

public class Plant extends GameObject {
	private int Age = 0, Max_Size_Age = -1, MAX_SIZE, MAX_SEED_NUMBER, MAX_SEED_CAST_DISTANCE;
	private double GROWTH_RATE, SEED_VIABILITY, P_DIAMETER;
	private GameObjectManager game=GameObjectManager.getInstance();
	private UUID uuid; 
	// Constructor. 
	public Plant(int x, int y, double g, double d, int maxSize, int seedNumber, int castDistance, double seedViability) {
		X_POS = x;
		Y_POS = y;
		GROWTH_RATE=g;
		P_DIAMETER = d;
		MAX_SIZE = maxSize;
		MAX_SEED_NUMBER = seedNumber;
		MAX_SEED_CAST_DISTANCE = castDistance;
		SEED_VIABILITY=seedViability;
		uuid = UUID.randomUUID();
	}
	// run this every second of simulation time. Returns an ArrayList<Plant> that may contain Plant objects if plant seeded.
	public ArrayList<Plant> Run() {
		Age++;
		ArrayList<Plant> newSeeds = new ArrayList<Plant>();
		if (!Seed()) {
			if (Age > 10) {
				Grow();
			} else if (Age == 10  && P_DIAMETER<1 ) {
				 	P_DIAMETER = .02;
			}
		} else {
			int numSeeds = (int)(Math.random()*MAX_SEED_NUMBER);
			for (int i = 0; i < numSeeds; i++) {
				//create random placement for new seed
				double r;
				double a;
				double x;
				double y;
				do {
					r = Math.random()*MAX_SEED_CAST_DISTANCE;
					a = Math.random()*2*Math.PI;
					x = X_POS+(int)(r*Math.cos(a));
					y = Y_POS+(int)(r*Math.sin(a));
				} while (x > game.getInstance().getMaxX() || x < 0 || y > game.getInstance().getMaxY() || y < 0);
				ArrayList<Obstacle> blocks = game.getInstance().getObstacles();
				boolean rockedSeed = false;
				for(Obstacle o : blocks) {
					if (o.GetXPos()-(o.getDiameter()/2) < x && o.GetXPos()+(o.getDiameter()/2) > x 
							&& o.GetYPos()-(o.getDiameter()/2) < y && o.GetYPos()+(o.getDiameter()/2) > y) {
						rockedSeed = true;
					}
				}
				if(!rockedSeed) {
					Plant p = new Plant((int)Math.round(x), (int)Math.round(y), game.getInstance().getPlantGrowthRate(), 0, MAX_SIZE, MAX_SEED_NUMBER, MAX_SEED_CAST_DISTANCE, SEED_VIABILITY);
					newSeeds.add(p);
				}
			}
			newSeeds = new ArrayList<Plant>(newSeeds.subList(0,(int)(SEED_VIABILITY*numSeeds)));
		}
		return newSeeds;
	}
	// Used for plants created during initial map creation. Makes sure the plants grows immediately.
	public void setAge() {
		Age = 11;
	}
	public double getDiameter() {
		return P_DIAMETER;
	}
	public void reduceDiameter(double x) {
		P_DIAMETER=P_DIAMETER-x;
	}
	public int getAge()
	{
		return Age;
	}
	// Returns
	private void Grow(){
		if (P_DIAMETER < MAX_SIZE) {
			P_DIAMETER += (MAX_SIZE*GROWTH_RATE/60);
			ArrayList<Obstacle> blocks = game.getInstance().getObstacles();
			double minDia = P_DIAMETER;
			for(Obstacle o : blocks) {
				double distance = Math.hypot(X_POS-o.GetXPos(), Y_POS-o.GetYPos());
				if (distance < ((P_DIAMETER/2)+(o.getDiameter()/2)) && (distance-(o.getDiameter()/2)) < minDia/2) {
					minDia = (distance-(o.getDiameter()/2))*2;
					if (Age>=Max_Size_Age)
						Max_Size_Age = Age;
				}
			}
			P_DIAMETER = minDia;
			if (P_DIAMETER >= MAX_SIZE) {
				P_DIAMETER = MAX_SIZE;
				if (Age>=Max_Size_Age)
					Max_Size_Age = Age;
			}
		}
	}
	private boolean Seed() {
		boolean hour_passed = ((Age-Max_Size_Age) % 3600 == 0 );
		if (hour_passed && P_DIAMETER >= MAX_SIZE) {
			return true;
		}
		return false;
	}
	public UUID getUUID(){
		return uuid;
	}
	
}