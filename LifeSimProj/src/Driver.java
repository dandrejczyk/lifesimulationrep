
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


public class Driver extends Application{
	
	private static int delay = 1000;
	private static UpdateThread updateT = new UpdateThread(1000);
	
	/**
     * Override the start method from Application
     * to create UI on application start-up.
     */
    @Override
    public void start(Stage primaryStage) {
    	// WebView webView = new WebView();
        // WebEngine webEngine = webView.getEngine();
        // webEngine.load( "C:\\Users\\ghosh\\BitBucket_Repo\\lifesimulationrep\\LifeSimProj\\bin\\Start_LifeformSim.html" );
        
    	SimView mainView = new SimView();
    	@SuppressWarnings("unused")
		SimController viewController = new SimController(mainView);
    	
        primaryStage.setTitle("Life Simulation");
        primaryStage.setScene(mainView.getView());
        primaryStage.show();
 
    }
	
	/**
	 * Allows user to increase speed by multiplier.
	 * 
	 * @param multiplier	The multiplier for the speed.
	 * 						Can be 1x, 10x, 50x, or 100x. Invalid
	 * 						multipliers will generate a usage error.
	 */
	public static void updateSpeed(String multiplier)
	{		
		if(multiplier == "1x")
		{
			delay = 1000;
			updateT.updateDelay(delay);
		} else if(multiplier == "10x")
		{
			delay = 100;
			updateT.updateDelay(delay);
		} else if(multiplier == "50x")
		{
			delay = 20;
			updateT.updateDelay(delay);
		} else if(multiplier == "100x")
		{
			delay = 10;
			updateT.updateDelay(delay);
		} else
		{
			System.err.println("Error: Invalid speed multiplier entered to update speed!");
		}
	}
	
	/**
	 * Send off application update thread to begin simulation.
	 */
	public static void startSim()
	{
		// If application thread isn't already running, start it!
		if(updateT.getState() == Thread.State.NEW)
		{
			updateT.start();
		} else if(updateT.getState() == Thread.State.TERMINATED)
		{
			System.err.println("Starting new thread");
			updateT = new UpdateThread(delay);
			updateT.start();
		}
	}
	
	/**
	 * Stop the application thread if it is still alive.
	 */
	public static void stopSim()
	{
		if(updateT.isAlive())
		{
			updateT.shutDown();
		}
	}
	
	public static void main(String args[])
	{
		
		GameObjectManager game=GameObjectManager.getInstance();		
		
		launch(args);
		
		updateT.shutDown();
		
		try
		{
			updateT.join();
		} catch (InterruptedException e)
		{
			System.err.println("Interrupted while trying to join update thread!");
		}
		
	}

}
