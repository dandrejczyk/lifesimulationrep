
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ComboBoxBase;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

/**
 * 
 * @author Daniel Andrejczyk
 * 
 * This class creates and updates all UI parts of the application.
 * This is the view class of the Model-View-Controller design
 * pattern used for this application. Therefore, this class does
 * not handle all the controls and does not directly update
 * simulation data, but rather observes the GameObjectManager
 * class and strategizes with the SimController class.
 */
public class SimView {
	
	// Scene width and height
	private final int SCENE_WIDTH = 1300;
	private final int SCENE_HEIGHT = 900;
	
	// Simulation view canvas width and height
	private final int SIM_VIEW_WIDTH = 1100;
	private final int SIM_VIEW_HEIGHT = 850;
	
	// The simulation speed options that can be selected
	private final ObservableList<String> simSpeeds =
		FXCollections.observableArrayList(
			"1x",
			"10x",
			"50x",
			"100x"
	);
	
	/*
	 * Components
	 */
	
	// Group top-level objects together to add to scene.
	private Group root = new Group();
	
	// The main scene to set and show once all components
	// have been added.
	private Scene scene = new Scene(root, SCENE_WIDTH, SCENE_HEIGHT, Color.LIGHTGRAY);;
	
	// Using a BorderPane layout scheme.
	// Left will be a menu of interactive buttons.
	// Right will be a dynamic overview.
	private BorderPane bp = new BorderPane();
	
	// Vertical Box to hold simulation buttons and labels.
	private VBox simButtons = new VBox();
    
    // Upload label and button.
    private Label uploadedFile = new Label("LifeSimulation01.xml");
    private Button uploadBtn = new Button("Upload!");
    
    // Start and Create Report Buttons.
    private Button startBtn = new Button("Start");
    private Button createReportBtn = new Button("Create Report");  
    
    // The overview section.
    // A canvas with a grid rendered to it is placed in the background.
    // Graphics Context gc used to realize grid.
    // A lifeforms Pane created to realize lifeforms on top of the canvas.
    // Lifeforms are redrawn on the frame with their new positions and sizes
    // every update.
	private Canvas canvas = new Canvas(SIM_VIEW_WIDTH, SIM_VIEW_HEIGHT);
	private GraphicsContext gc = canvas.getGraphicsContext2D();
	private static Pane lifeforms = new Pane();
	private Group overview = new Group();
	
	// Combo box of update speed selections to choose from.
	final ComboBox<String> speedMult = new ComboBox<String>(simSpeeds);
	
	//Creating a dialog
    private static Alert alert = new Alert(AlertType.INFORMATION);
	
	/**
	 * Creates an instance of this view.
	 */
	public SimView()
	{
		createView();
	}
	
	/**
	 * Creates the view for the scene.
	 */
	private void createView()
	{
		
		// Set spacing and add buttons to left interactive panel.
        simButtons.setSpacing(5);
        simButtons.getChildren().addAll(uploadedFile, uploadBtn, speedMult, startBtn, createReportBtn);
        
        // Set default speed to 1x
    	speedMult.getSelectionModel().selectFirst();

    	// Group overview componenets
        overview.getChildren().addAll(canvas, lifeforms);
                
        // Render the grid to the background canvas
        realizeGrid(gc);
        
        // Set border pane components and margins
        bp.setRight(overview);
        BorderPane.setMargin(overview, new Insets(10,10,10,10));
        bp.setLeft(simButtons);
        BorderPane.setMargin(simButtons, new Insets(10,10,10,10));
        
        // Set up simulation information pop-up.
        alert.setTitle("End of Simulation");
        //Setting the header text of the alert.
        alert.setHeaderText("Hello World!");
                
        // Add border pane
        root.getChildren().add(bp);
		
	}
    
    /*
     * Getter Methods
     */
	
	/**
	 * @return 	ComboBox<String>	The simulation speed selector combo box.
	 */
    public ComboBox<String> getSpeedSelector()
    {
    	return speedMult;
    }
    
    /**
	 * @return 	Button	The file upload button.
	 */
	public Button getFileUploadBtn() {
		return uploadBtn;
	}
	
	/**
	 * @return 	Button	The file upload button.
	 */
	public Label getFileUploadLabel() {
		return uploadedFile;
	}
	
	/**
	 * @return 	Button	The create report.
	 */
	public Button getReportBtn() {
		return createReportBtn;
	}
	
	/**
	 * @return 	Button	The file upload button.
	 */
	public Button getSimStartBtn() {
		return startBtn;
	}
    
	/**
	 * @return 	Scene	The simulation view.
	 */
    public Scene getView()
    {
    	return scene;
    }
    
    /**
     * Removes old graphics from the lifeforms Pane. This includes graphics
     * to represent the old positions of obstacles, plants, grazers, and predators.
     */
    public static void clearLifeForms()
    {
    	lifeforms.getChildren().clear();
    }
    
    public static void displayEOSMessage(String lifeform)
    {
    	alert.setHeaderText("Simulation has ended because lifeform " + lifeform + " reached 0!");
    	alert.setContentText("Upload a new file to begin a new simulation.");
    	alert.showAndWait();
    }

	/**
     * Renders an obstacle to the canvas.
     * 
     * @param xPos		The x position of the obstacle relative to the top
     * 					left corner.
     * @param yPos		The y position of the obstacle relative to the top
     * 					left corner.
     * @param diameter	The diameter of the obstacle.
     */
    public static void realizeObstacle(int xPos, int yPos, int diameter)
    {

    	Circle obstacle = new Circle(diameter / 2, Color.GRAY);
    	// Add 49 to account for the grid offset within the canvas.
    	// Grid was offset from top-left corner to allow for gridlines,
    	// ticks, and axis labeling/numbering.
    	obstacle.setCenterX(xPos + 49);
    	obstacle.setCenterY(yPos + 49);
    	lifeforms.getChildren().add(obstacle);
    }
    
    /**
     * Renders a plant to the canvas.
     * 
     * @param xPos		The x position of the plant relative to the top
     * 					left corner.
     * @param yPos		The y position of the plant relative to the top
     * 					left corner.
     * @param diameter	The diameter of the plant.
     */
    public synchronized static void realizePlant(int xPos, int yPos, double diameter)
    {
    	Circle plant = new Circle(diameter / 2, Color.GREEN);
    	// Add 49 to account for the grid offset within the canvas.
    	// Grid was offset from top-left corner to allow for gridlines,
    	// ticks, and axis labeling/numbering.
    	plant.setCenterX(xPos + 49);
    	plant.setCenterY(yPos + 49);
    	lifeforms.getChildren().add(plant);
    }
    
    /**
     * Renders a grazer to the canvas.
     * 
     * @param xPos		The x position of the grazer relative to the top
     * 					left corner.
     * @param yPos		The y position of the grazer relative to the top
     * 					left corner.
     */
    public static void realizeGrazer(int xPos, int yPos)
    {
    	Rectangle grazer = new Rectangle(10, 10, Color.MAROON);
    	// Add 44 to x position and y position to account for the grid offset 
    	// within the canvas. Grid was offset from top-left corner to allow 
    	// for gridlines, ticks, and axis labeling/numbering.
    	grazer.setX(xPos + 44);
    	grazer.setY(yPos + 44);
    	lifeforms.getChildren().add(grazer);
    }
    
    /**
     * Renders a predator to the canvas.
     * 
     * @param xPos		The x position of the predator relative to the top
     * 					left corner.
     * @param yPos		The y position of the predator relative to the top
     * 					left corner.
     */
    public static void realizePredator(int xPos, int yPos)
    {
    	Rectangle predator = new Rectangle(10, 10, Color.RED);
    	// Add 44 to x position and y position to account for the grid offset 
    	// within the canvas. Grid was offset from top-left corner to allow 
    	// for gridlines, ticks, and axis labeling/numbering.
    	predator.setX(xPos + 44);
    	predator.setY(yPos + 44);
    	lifeforms.getChildren().add(predator);
    }
    
    /**
     * Realizes the background axes, axes labels, and grid on which to render the scene.
     * 
     * @param gc	The graphics context used to draw t1000he grid.
     */
    private void realizeGrid(GraphicsContext gc)
    {
    	
    	gc.setFill(Color.WHITE);
        gc.fillRect(50, 50, SIM_VIEW_WIDTH - 100, SIM_VIEW_HEIGHT - 100);
        
        gc.setLineWidth(2);
        gc.setStroke(Color.BLACK);
        
        // Top and left side border lines
        gc.strokeLine(49, 49, SIM_VIEW_WIDTH - 51, 49);
        gc.strokeLine(49, 49, 49, SIM_VIEW_HEIGHT - 51);
        
        for(int xTick = 0; xTick <= SIM_VIEW_WIDTH - 100; xTick += 25)
        {
        	// x axis ticks
        	gc.strokeLine(49 + xTick, 45, 49 + xTick, 54);
        	
        	// x axis tick labels
        	gc.strokeText(Integer.toString(xTick), 40 + xTick, 40);
    
        	gc.save();
        	
        	// x grid lines
        	gc.setStroke(Color.LIGHTGRAY);
        	gc.strokeLine(74 + xTick, 54, 74 + xTick, SIM_VIEW_HEIGHT - 51);
        	
        	gc.restore();
        	
        }
    	
        for(int yTick = 0; yTick <= SIM_VIEW_HEIGHT - 100; yTick += 25)
        {
        	// x axis ticks
        	gc.strokeLine(45, 49 + yTick, 54, 49 + yTick);
        	
        	// x axis tick labels
        	gc.strokeText(Integer.toString(yTick), 15, 55 + yTick);
    
        	gc.save();
        	
        	// x grid lines
        	gc.setStroke(Color.LIGHTGRAY);
        	gc.strokeLine(54, 74 + yTick, SIM_VIEW_WIDTH - 51, 74 + yTick);
        	
        	gc.restore();
        	
        }
    }

}