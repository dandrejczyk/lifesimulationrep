
import java.util.ArrayList;
import java.util.UUID;

public class GameObject {
	protected double X_POS;
	protected double Y_POS;
	
	public int GetXPos() {
		return (int)Math.round(X_POS);
	}
	public int GetYPos() {
		return (int)Math.round(Y_POS);
	}
	
}
