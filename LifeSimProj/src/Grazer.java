
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.lang.Math;
import java.util.Arrays;
public class Grazer extends GameObject {
	int ENERGY_INPUT, ENERGY_OUTPUT, ENERGY_TO_REPRODUCE,//Instance variables in all caps are pulled from the Simulation File
		G_ENERGY_LEVEL, deleted, timeEating=0, timeMoved, moveTime=0, lastDrop, fleeTime;
	/*
	 * Deleted: flag that trips if grazer energy falls below zero 
	 * timeEating: Represents how many game seconds the grazer has been eating,
	 * 	used to determine if the grazer should kill the plant and move on to the next
	 * timeMoved: Represents how many seconds the grazer has been moving, used to calculate when to lower it's energy level
	 * moveTime: Used to show how long the grazer has been moving in a certain direction
	 
	 */
	double MAINTAIN_SPEED, MAX_SPEED, distanceMoved=0, lastStand=-1;
	boolean hunting=false, fleeing=false;
	private Plant food;
	private UUID uuid = UUID.randomUUID();
	private int[] randLoc = new int[] {0,0};
	private GameObjectManager game=GameObjectManager.getInstance();
	/*
	 *distanceMoved : Represents how far  the grazer has moved, used to calculate when to lower it's energy level
	 * hunting: Flag used to show when grazer is moving towards food
	 * food: Copy of current plant grazer is eating
	 * uuid: Unique identifier for each grazer, used for tracking
	 * randLoc: Used to store old location for calculation of random location for travel when no objects of interest are found in scan
	 * game: reference to GameObjectManager singleton instance
	 */ 
	/**
	 * @param eNERGY_INPUT
	 * @param eNERGY_OUTPUT
	 * @param eNERGY_TO_REPRODUCE
	 * @param g_ENERGY_LEVEL
	 * @param mAINTAIN_SPEED
	 * @param mAX_SPEED
	 * @param xpos
	 * @param ypos
	 */
	public Grazer(int eNERGY_INPUT, int eNERGY_OUTPUT, int eNERGY_TO_REPRODUCE, int g_ENERGY_LEVEL,
			double mAINTAIN_SPEED, double mAX_SPEED, int xpos, int ypos) {
		X_POS=(double)xpos;
		Y_POS=(double)ypos;
		ENERGY_INPUT = eNERGY_INPUT;
		ENERGY_OUTPUT = eNERGY_OUTPUT;
		ENERGY_TO_REPRODUCE = eNERGY_TO_REPRODUCE;
		G_ENERGY_LEVEL = g_ENERGY_LEVEL;
		MAINTAIN_SPEED = mAINTAIN_SPEED;
		MAX_SPEED = mAX_SPEED;
	}
	/**
	 * Run function Called every game tick
	 * @return list of grazers it would reproduce if it's able to
	 */
	public ArrayList<Grazer> Run() {
		//System.out.println(X_POS);
		/*int[] loc = new int[] {500,500};
		Travel2(loc);*/
		//System.out.println(G_ENERGY_LEVEL);
		ArrayList<Grazer> newGrazers = new ArrayList<Grazer>(); //List to store new grazers made if there are any
		if ((int)distanceMoved % 5 == 0 && (int)distanceMoved!=lastDrop) //Lowers energy every 5 DU mvoed
		{
			lastDrop=(int)distanceMoved;
			G_ENERGY_LEVEL-=ENERGY_OUTPUT;
		}
		if (G_ENERGY_LEVEL<= 0)//Sets flag for deletion if energy drops below 0
		{
			deleted=1;
		}
		if(G_ENERGY_LEVEL < 25 && lastStand==-1)//Checks if energy drops below 25, and prevents grazer from moving 10 DU after that
			lastStand=distanceMoved;
		if(distanceMoved>=lastStand+10 && lastStand!=-1)//Checks if energy drops below 25, and prevents grazer from moving 10 DU after that
			MAX_SPEED=0;
		if(G_ENERGY_LEVEL>ENERGY_TO_REPRODUCE)//Reproduces if energy is above reproduction level, then lowers energy by half
		{
			for (int i = 0; i < 2; i++) {//Loops twice because two grazers are produced on reproduction
				double x;
				double y;
				do {////create random placement for new grazer, making sure they don't fall out of bounds
					x = X_POS+((Math.random()*5)+1);
					y = Y_POS+((Math.random()*5)+1);
				} while (x > game.getInstance().getMaxX() || x < 0 || y > game.getInstance().getMaxY() || y < 0);//Checking new grazer isn't out of bounds
				Grazer g = new Grazer(ENERGY_INPUT, ENERGY_OUTPUT, ENERGY_TO_REPRODUCE, G_ENERGY_LEVEL/2,//Creating new Grazer
						MAINTAIN_SPEED, MAX_SPEED,(int)Math.round(x), (int)Math.round(y));
				newGrazers.add(g);//Add to list
			}
			G_ENERGY_LEVEL/=2;//Half current grazer's energy
		}
		if (hunting) {//Hunts for food
			Hunt(food);
		} else {
			int[] loc = Scan2();//Scan function returns coordinates of interest and stores it in loc
			
			int[] noloc = new int[] {-1,-1};//Basically null location to see if the scan function returned no cooridates of interest
			if (Arrays.equals(loc, noloc)) {//Checks if the grazer has been moving in the same direction randomly for 60 seconds or not
				moveTime++;					// and changes the direction if it has
				if (moveTime <60) {
					loc = randLoc;
				} else { //Chooses random coordinates for the grazer to move to if there is nothing on scan
					loc[0] = (int)(Math.random()*game.getInstance().getMaxX());
					loc[1] = (int)(Math.random()*game.getInstance().getMaxY());
					moveTime = 0;
				}
			}
			if (hunting) {
				Hunt(food);
			}
			else {
				Travel2(loc);//Travels to location of interest(Either to food or away from predator)
			} 
		}
		return newGrazers;//Returns list of grazers produced to GameObjectManager to add to grazerList
	
	}
	// Travel3 is the base of Travel, is does not check for anything really
	private void Travel3(int[] loc) {
		double travelAngle;
		if(loc[1] == Y_POS && loc[0] == X_POS) {
			return;
		}
		if(loc[1] == Y_POS) {
			travelAngle = 0;
		} else if (loc[0] == X_POS) {
			travelAngle = Math.PI/2;
		} else {
			travelAngle = Math.atan((double)(loc[0]-X_POS)/(double)(loc[1]-Y_POS));
		}
		double speed = MAX_SPEED/60;
		if(loc[0]<X_POS)
		{
			X_POS = (int)(X_POS-Math.ceil(speed)*Math.cos(travelAngle));
		}
		else
		{
			X_POS = (int)(X_POS+Math.ceil(speed)*Math.cos(travelAngle));
		}
		
		if(loc[1]<Y_POS)
		{
			Y_POS = (int)(Y_POS-Math.ceil(speed)*Math.sin(travelAngle));
		}
		else
		{
			Y_POS = (int)(Y_POS+Math.ceil(speed)*Math.sin(travelAngle));
		}
	}
	//Travel2 is a copied version of Travel, without any of the commented out stuff. It works better than Travel for some reason.
	private void Travel2(int[] loc) {
		if (game.getInstance().getMaxX() < loc[0] || loc[0] < 0 ) {//Bounds checking-Checks if coords supplied are out of bounds
			loc[0] = (int)Math.round(X_POS);
		}
		if (game.getInstance().getMaxY() < loc[1] || loc[1] < 0) {
			loc[1] = (int)Math.round(Y_POS);
		}
		if(game.getInstance().getMaxX() == (int)Math.round(X_POS) || (int)Math.round(X_POS) == 0 )//More bounds checking - Check if current grazer position is at bounds
		{
			loc[0] = (int)(Math.random()*game.getInstance().getMaxX());
		}

		if(game.getInstance().getMaxY() == (int)Math.round(Y_POS) || (int)Math.round(Y_POS) == 0 )
		{
			loc[1] = (int)(Math.random()*game.getInstance().getMaxY());
		}
		double travelAngle;//Travel Angle variable
		if(loc[1] == (int)Math.round(Y_POS) && loc[0] == (int)Math.round(X_POS)) {//Exits function if travel leads to same location
			return;
		}
		/*if(loc[1] == (int)Math.round(Y_POS)) {
			travelAngle = 0;
		} else if (loc[0] == (int)Math.round(X_POS)) {
			travelAngle = Math.PI/2;
		} else {*/
			travelAngle = Math.atan((double)(loc[1]-Y_POS)/(double)(loc[0]-X_POS));//Gets the travel angle between current position and coordinates
			//travelAngle = Math.atan((double)(loc[0]-X_POS)/(double)(loc[1]-Y_POS));
		//}
		double speed = MAX_SPEED/60;//Sets the speed in seconds
		if(!fleeing)//If the grazer is fleeing, it gets its Max Speed for MAINTAIN_SPEED minutes, otherwise it's at 75% of it's normals speed
		{
			speed = (MAX_SPEED/60)*0.75;
		}
		else if (fleeing && !(fleeTime > (MAINTAIN_SPEED*60))) {
			speed = MAX_SPEED/60;
			fleeTime++;
		}
		//Variables to hold the Old X and Y variables for distance calculation
		double oldx;
		double oldy;
		//Variables to do calculations on New X and Y variables.
		double newX;
		double newY;
		//Sets newX and newY to the correct coordinates using 
		if (loc[0] < X_POS) {
			newX = X_POS - speed*Math.cos(travelAngle);
			newY = Y_POS - speed*Math.sin(travelAngle);
		} else {
			newX = X_POS + speed*Math.cos(travelAngle);
			newY = Y_POS + speed*Math.sin(travelAngle);
		}
		ArrayList<Obstacle> oList = game.getInstance().getObstacles();
		boolean block1 = false;
		boolean block2 = false;
		boolean block3 = false;
		for( Obstacle o : oList) {
			if ((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				block1 = true;
			}
			if ((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (Y_POS <= o.GetYPos() + (o.getDiameter()/2)) && (Y_POS >= o.GetYPos() - (o.getDiameter()/2))) {
				block2 = true;
			}
			if ((X_POS <= o.GetXPos() + (o.getDiameter()/2)) && (X_POS >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				block3 = true;
			}
		}
		if(block1) {
			if (block2) {
				if(block3) {
					if (loc[0] < X_POS) {
						newX = X_POS + speed*Math.cos(travelAngle);
						newY = Y_POS + speed*Math.sin(travelAngle);
					} else {
						newX = X_POS - speed*Math.cos(travelAngle);
						newY = Y_POS - speed*Math.sin(travelAngle);
					}
				} else {
					newX = X_POS;
					if(loc[1] < Y_POS) {
						newY = Y_POS - speed;
					} else {
						newY = Y_POS + speed;
					}
				}
			} else {
				newY = Y_POS;
				if(loc[0] < X_POS) {
					newX = X_POS - speed;
				} else {
					newX = X_POS + speed;
				}
			}
		}
		/*if(loc[0]<X_POS)
		{
			newX = X_POS-speed*Math.cos(travelAngle);
		}
		else
		{
			newX = X_POS+speed*Math.cos(travelAngle);
		}
		
		if(loc[1]<Y_POS)
		{
			newY = Y_POS-speed*Math.sin(travelAngle);
		}
		else
		{
			newY = Y_POS+speed*Math.sin(travelAngle);
		}*/
		oldx=X_POS;
		if ((newX < loc[0] && X_POS > loc[0]) || (newX > loc[0] && X_POS < loc[0])) {
			newX = loc[0];
		}
		if (!(newX > game.getInstance().getMaxX() || newX<0)) {
			X_POS = newX;
		}
		oldy=Y_POS;
		if ((newY < loc[1] && Y_POS > loc[1]) || (newY > loc[1] && Y_POS < loc[1])) {
			
			newY = loc[1];
		}
		if (!(newY > game.getInstance().getMaxY() || newY<0)) {
			Y_POS = newY;
		}
		double distance = Math.sqrt(Math.pow(oldx-X_POS,2)+Math.pow(oldy-Y_POS,2));
		distanceMoved+=distance;
		timeMoved++;
		randLoc = loc;
		//System.out.println(X_POS+" "+Y_POS);
	}
	public int getEnergy() {
		return G_ENERGY_LEVEL;
	}
	private void Eat(Plant g) {
		timeEating++;
		//System.out.println("Qfqwegbuiqwevghui");
		//System.out.println(timeEating);
		if (timeEating%20==0) {
			g.reduceDiameter(1);
		}
		if (timeEating%60==0)
		{
			G_ENERGY_LEVEL+=ENERGY_INPUT;
		}
		if(timeEating%600==0)
		{
			for(int i=0; i < game.getInstance().getPlants().size(); i++)
			{
				
				//double dist=Math.hypot((super.GetXPos()-g.GetXPos()),(super.GetYPos()-g.GetYPos()));
				timeEating=0;
				if (g.getUUID().compareTo(game.getInstance().getPlants().get(i).getUUID())==0)
				{
					game.getInstance().getPlants().remove(i);
					break;
				}
			}
			
			
			
		}
		
	}
	public boolean Reproduce() {
		if (G_ENERGY_LEVEL >= ENERGY_TO_REPRODUCE) {
			return true;
		}
		return false;
	}
	/*private void Travel(int[] loc) {
		if (game.getInstance().getMaxX() < loc[0] || loc[0] < 0 ) {
			loc[0] = X_POS;
		}
		if (game.getInstance().getMaxY() < loc[1] || loc[1] < 0) {
			loc[1] = Y_POS;
		}
		if(game.getInstance().getMaxX() == X_POS || X_POS == 0 )
		{
			
			loc[0] = (int)(Math.random()*game.getInstance().getMaxX());
		}

		if(game.getInstance().getMaxY() == Y_POS || Y_POS == 0 )
		{
			loc[1] = (int)(Math.random()*game.getInstance().getMaxY());
		}
		double travelAngle;
		if(loc[1] == Y_POS && loc[0] == X_POS) {
			return;
		}
		if(loc[1] == Y_POS) {
			travelAngle = 0;
		} else if (loc[0] == X_POS) {
			travelAngle = Math.PI/2;
		} else {
			travelAngle = Math.atan((double)(loc[0]-X_POS)/(double)(loc[1]-Y_POS));
		}
		//double travelAngle = Math.atan((double)(loc[0]-X_POS)/(double)(loc[1]-Y_POS));
		double speed = MAX_SPEED/60;
		if (timeMoved > (MAINTAIN_SPEED*60)) {
			speed = (MAX_SPEED/60)*0.75;
			if (speed == 0) {
				timeMoved = 0;
				return;
			}
		}
		//System.out.println("Travel Angel"+travelAngle);
		int oldx;
		int oldy;
		double newX;
		double newY;
		//System.out.println(speed);
		if(loc[0]<X_POS)
		{
			newX = (X_POS-Math.ceil(speed)*Math.cos(travelAngle));
		}
		else
		{
			newX = (X_POS+Math.ceil(speed)*Math.cos(travelAngle));
		}
		
		if(loc[1]<Y_POS)
		{
			newY = (Y_POS-Math.ceil(speed)*Math.cos(travelAngle));
		}
		else
		{
			newY = (Y_POS+Math.ceil(speed)*Math.sin(travelAngle));
		}
		
		//System.out.println(speed);
		/*ArrayList<Obstacle> oList = game.getInstance().getObstacles();
		boolean block1 = false;
		boolean block2 = false;
		boolean block3 = false;
		for( Obstacle o : oList) {
			if ((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				block1 = true;
			}
			if ((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (Y_POS <= o.GetYPos() + (o.getDiameter()/2)) && (Y_POS >= o.GetYPos() - (o.getDiameter()/2))) {
				block2 = true;
			}
			if ((X_POS <= o.GetXPos() + (o.getDiameter()/2)) && (X_POS >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				block3 = true;
			}
		}
		if (block1) {
			if(block2) {
				newX = X_POS;
				if(block3) {
					newY = Y_POS;
				}
			} else {
				newY = Y_POS;
			}
		}
		/*ArrayList<Obstacle> oList = game.getInstance().getObstacles();
		for( Obstacle o : oList) {
			while((newX <= o.GetXPos() + (o.getDiameter()/2)) && (newX >= o.GetXPos() - (o.getDiameter()/2)) 
					&& (newY <= o.GetYPos() + (o.getDiameter()/2)) && (newY >= o.GetYPos() - (o.getDiameter()/2))) {
				travelAngle = (Math.random()*Math.PI/2)-Math.PI/4;
				newX = (int)(X_POS+speed*Math.cos(travelAngle));
				newY = (int)(Y_POS+speed*Math.sin(travelAngle));
			}
		}
		
		/*ArrayList<Plant> pList = game.getInstance().getPlants();
		for( Plant p : pList) {
			while((newX <= p.GetXPos() + (p.getDiameter()/2)) && (newX >= p.GetXPos() - (p.getDiameter()/2)) 
					&& (newY <= p.GetYPos() + (p.getDiameter()/2)) && (newY >= p.GetYPos() - (p.getDiameter()/2))) {
				travelAngle = (Math.random()*Math.PI/2)-Math.PI/4;
				newX = (int)(X_POS+speed*Math.cos(travelAngle));
				newY = (int)(Y_POS+speed*Math.sin(travelAngle));
			}
		}
		//System.out.println("X: "+newX+"Y: "+newY);
		oldx = X_POS;
		if ((newX < loc[0] && X_POS > loc[0]) || (newX > loc[0] && X_POS < loc[0])) {
			newX = loc[0];
		}
		if (!(newX > game.getInstance().getMaxX() || newX<0)) {
			X_POS = (int)Math.round(newX);
		} 
		oldy=Y_POS;
		if ((newY < loc[1] && Y_POS > loc[1]) || (newY > loc[1] && Y_POS < loc[1])) {
			newY = loc[1];
		}
		if (!(newY > game.getInstance().getMaxY() || newY<0)) {
			Y_POS = (int)Math.round(newY);
		} 
		//System.out.println("X: "+X_POS);
		//System.out.println("Y: "+Y_POS);
		//System.out.println("OLD x: " +oldx+"Old Y: "+oldy +"New X: "+X_POS+"New Y: "+Y_POS);
		double distance = Math.sqrt(Math.pow(oldx-X_POS,2)+Math.pow(oldy-X_POS,2));
		distanceMoved+=distance;
		timeMoved++;
		System.out.println(X_POS+" "+Y_POS);
	}*/
	private int[] Scan2() {
		int[] loc = new int[] {-1,-1};
		int[] noloc = new int[] {-1,-1};
		ArrayList<GameObject> blocks = new ArrayList<GameObject>();
		ArrayList<Plant> plList = game.getInstance().getPlants();
		ArrayList<Obstacle> oList = game.getInstance().getObstacles();
		ArrayList<Predator> pList = game.getInstance().getPredators();
		double minDistance=10000;
		for (Obstacle o : oList) {
			double distance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(o);
			}
		}
		for (Predator p : pList) {
			double distance = Math.hypot(p.GetXPos()-X_POS, p.GetYPos()-Y_POS);
			if (p.getUUID() != uuid) {
				if (distance < 25 && distance < minDistance && p.isHunting()) {
					boolean blocked = false;
					for (GameObject o: blocks) {
						double oRadius = 0;
						if (o instanceof Plant) {
							oRadius = ((Plant)o).getDiameter()/2;
						} else if (o instanceof Obstacle) {
							oRadius = ((Obstacle)o).getDiameter()/2;
						}
						double baX = p.GetXPos() - X_POS;
						double baY = p.GetYPos() - Y_POS;
						double caX = o.GetXPos() - X_POS;
						double caY = o.GetYPos() - Y_POS;
						double a = baX * baX + baY * baY;
						double bBy2 = baX * caX + baY * caY;
						double c = caX * caX + caY * caY - oRadius*oRadius;
						double pBy2 = bBy2 / a;
						double q = c/a;
						double blockDistance = Math.hypot(o.GetXPos()-X_POS,o.GetYPos()-Y_POS);
						if (pBy2 * pBy2 - q < 0 && blockDistance <= distance) {
							blocked = true;
						}
					}
					if (!blocked) {
						fleeing=true;
						int xRun = (int)Math.round(X_POS-10*(p.GetXPos()-X_POS));
						int yRun = (int)Math.round(Y_POS-10*(p.GetYPos()-Y_POS));
						double travelAngle = Math.atan((double)(yRun-Y_POS)/(double)(xRun-X_POS));
						if (xRun < X_POS) {
							loc[0] = (int)Math.round(X_POS - 5*Math.cos(travelAngle));
							loc[1] = (int)Math.round(Y_POS - 5*Math.sin(travelAngle));
						} else {
							loc[0] = (int)Math.round(X_POS + 5*Math.cos(travelAngle));
							loc[1] = (int)Math.round(Y_POS + 5*Math.sin(travelAngle));
						}
						minDistance = distance;
					}
				}
			}
		}
		if (Arrays.equals(loc, noloc)) {
			fleeing=false;
			for (Plant p : plList) {
				double distance = Math.hypot(p.GetXPos()-this.GetXPos(), p.GetYPos()-this.GetYPos());
				if (distance < 150 && distance < minDistance) {
					boolean blocked = false;
					for (GameObject o: blocks) {
						double oRadius = 0;
						if (o instanceof Plant) {
							oRadius = ((Plant)o).getDiameter()/2;
						} else if (o instanceof Obstacle) {
							oRadius = ((Obstacle)o).getDiameter()/2;
						}
						double baX = p.GetXPos() - X_POS;
						double baY = p.GetYPos() - Y_POS;
						double caX = o.GetXPos() - X_POS;
						double caY = o.GetYPos() - Y_POS;
						double a = baX * baX + baY * baY;
						double bBy2 = baX * caX + baY * caY;
						double c = caX * caX + caY * caY - oRadius*oRadius;
						double pBy2 = bBy2 / a;
						double q = c/a;
						double blockDistance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
						if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
							blocked = true;
						}
					}
					
					if (!blocked) {
						loc[0] = p.GetXPos();
						loc[1] = p.GetYPos();
						minDistance = distance;
						food = p;
						hunting = true;
					}
				}
			}
		}
		return loc;
	}
	private int[] Scan() {
		int[] loc = new int[] {-1,-1};
		int[] noloc = new int[] {-1,-1};
		ArrayList<GameObject> blocks = new ArrayList<GameObject>();
		ArrayList<Obstacle> oList = game.getInstance().getObstacles();
		ArrayList<Plant> plList = game.getInstance().getPlants();
		ArrayList<Predator> pList = game.getInstance().getPredators();
		double minDistance=10000;
		for (Obstacle o : oList) {
			double distance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(o);
			}
		}
		for (Plant pl : plList) {
			double distance = Math.sqrt(Math.pow(pl.GetXPos()-X_POS,2)+Math.pow(pl.GetYPos()-X_POS,2));
			if (distance < 150) {
				blocks.add(pl);
			}
		}
		/*for (Predator p : pList) {
			double distance = Math.sqrt(Math.pow(p.GetXPos()-X_POS,2)+Math.pow(p.GetYPos()-X_POS,2));
			if ( distance < minDistance && distance < 25 && p.isHunting()) {
					loc[0] = (int)Math.round(X_POS-10*(p.GetXPos()-X_POS));
					loc[1] = (int)Math.round(Y_POS-10*(p.GetYPos()-Y_POS));
					minDistance = distance;
			}
		}*/
		minDistance=10000;
		if (Arrays.equals(loc, noloc)) {
			for (Plant p: plList) {
				double distance = Math.sqrt(Math.pow(p.GetXPos()-X_POS,2)+Math.pow(p.GetYPos()-X_POS,2));
				if (distance < minDistance) {
					if (distance < 150) {
						boolean blocked = false;
						for (GameObject o: blocks) {
							double oRadius = 0;
							if (o instanceof Plant) {
								oRadius = ((Plant)o).getDiameter()/2;
							} else if (o instanceof Obstacle) {
								oRadius = ((Obstacle)o).getDiameter()/2;
							}
							double baX = p.GetXPos() - X_POS;
							double baY = p.GetYPos() - Y_POS;
							double caX = o.GetXPos() - X_POS;
							double caY = o.GetYPos() - Y_POS;
							double a = baX * baX + baY * baY;
							double bBy2 = baX * caX + baY * caY;
							double c = caX * caX + caY * caY - oRadius*oRadius;
							double pBy2 = bBy2 / a;
							double q = c/a;
							double blockDistance = Math.sqrt(Math.pow(o.GetXPos()-X_POS,2)+Math.pow(o.GetYPos()-X_POS,2));
							if (pBy2 * pBy2 - q < 0 && blockDistance < distance) {
								blocked = true;
							}
						}
						
						if (!blocked) {
							loc[0] = p.GetXPos();
							loc[1] = p.GetYPos();
							minDistance = distance;
							food = p;
							hunting = true;
						}
					}
					
				}
			}
		}
		return loc;
	}
	private void Hunt(Plant g) {
		food = g;
		//if ((int)Math.round(X_POS) == g.GetXPos() && (int)Math.round(Y_POS) == g.GetYPos()) {\
		double distance = Math.hypot(X_POS-g.GetXPos(),Y_POS-g.GetYPos());
		if (distance <= g.getDiameter()/2) {
			
			Eat(g);
			hunting = false;
		} else
		{
			int[] preyLoc = new int[] {g.GetXPos(),g.GetYPos()};
			Travel2(preyLoc);

		}
	}
	
	public UUID getUUID(){
		return uuid;
	}
	public int getDeleted() {
		return deleted;
	}
	public double getDistanceMoved() {
		return distanceMoved;
	}
	public int getFleeTime() {
		return fleeTime;
	}
	public boolean getFleeing() {
		return fleeing;
	}
}

