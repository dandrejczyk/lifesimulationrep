
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javafx.application.Platform;
public class GameObjectManager {

	/*
	 * Variables
	 */
	private static GameObjectManager single_instance = null;
	private File dataFile = new File(new String(System.getProperty("user.dir") + "/LifeSimulation01.xml"));
	
	private static final SimpleDateFormat sdf1 = new SimpleDateFormat("hh_mm_ss");
	
	private double maxY, maxX;
	private ArrayList<Plant> plantList;
	private ArrayList<Predator> predatorList;
	private ArrayList<Grazer> grazerList;
	private ArrayList<Obstacle> obstacleList;
	private final static GameObjectManager instance=new GameObjectManager();
	private double plantGrowthRate;
	LifeSimDataParser lsdp;
	
	private GameObjectManager(){
		
		plantList=new ArrayList<Plant>();
		predatorList=new ArrayList<Predator>();
		grazerList=new ArrayList<Grazer>();
		obstacleList=new ArrayList<Obstacle>();
		
		loadNewRun();
		
	}
	
	public static GameObjectManager getInstance()
    {
        /*if (single_instance == null)
            single_instance = new GameObjectManager();
 
        return single_instance;*/
		return instance;
    }	
	/**
	 * Prepares simulation to run for a new file upload.
	 */
	private void loadNewRun()
	{
		
		// Clear screen of previous lifeform graphics
		Platform.runLater(()-> {
			SimView.clearLifeForms();
		});
		
		plantList.clear();
		predatorList.clear();
		grazerList.clear();
		obstacleList.clear();

		int iVal = 0;
		int iPlantCount = 0, iGrazerCount = 0, iPredatorCount = 0, iObstacleCount = 0;
		double dVal = 0.0;
		lsdp = LifeSimDataParser.getInstance();	// Get the singleton
		lsdp.reset();
		lsdp.initDataParser(dataFile.getAbsolutePath());
		plantGrowthRate=lsdp.getPlantGrowthRate();
		maxX=lsdp.getWorldWidth();
		maxY=lsdp.getWorldHeight();


		iPlantCount = lsdp.getInitialPlantCount();
		//System.out.println(iPlantCount);
		iGrazerCount= lsdp.getInitialGrazerCount();
		iPredatorCount = lsdp.getInitialPredatorCount();
		iObstacleCount = lsdp.getObstacleCount();
		for(int i=0; i< iPlantCount; i++)
		{
			if(lsdp.getPlantData())
			{
				Plant p = new Plant(lsdp.PlantX, lsdp.PlantY, lsdp.getPlantGrowthRate(), lsdp.PlantDiameter, lsdp.getMaxPlantSize(), 
						lsdp.getMaxSeedNumber(),lsdp.getMaxSeedCastDistance(), lsdp.getSeedViability());
				plantList.add(p);
				Platform.runLater(()-> {
					SimView.realizePlant(p.GetXPos(), p.GetYPos(), (int)p.getDiameter());
				});
			}
			else
			{
				System.err.println("Failed to read data for plant " + i);
			}
	
		}
		
		
		for(int i=0; i< iGrazerCount; i++)
		{
			if(lsdp.getGrazerData())
			{
				Grazer g = new Grazer(lsdp.getGrazerEnergyInputRate(), lsdp.getGrazerEnergyOutputRate(),
						lsdp.getGrazerEnergyToReproduce(), lsdp.GrazerEnergy, 
						lsdp.getGrazerMaintainSpeedTime(),lsdp.getGrazerMaxSpeed(), lsdp.GrazerX, lsdp.GrazerY);
				grazerList.add(g);
				Platform.runLater(()-> {
					SimView.realizeGrazer(g.GetXPos(), g.GetYPos());
				});
			}
			else
			{
				System.err.println("Failed to read data for grazer " + i);
			}
		}
		for(int i=0; i< iPredatorCount; i++)
		{
			if(lsdp.getPredatorData())
			{
				String genes=lsdp.PredatorGenotype;
				String[] arr=genes.split(" ",0);

				Predator p = new Predator(lsdp.PredatorX, lsdp.PredatorY,
						arr[0],arr[1],arr[2], lsdp.getPredatorEnergyToReproduce(), 
						lsdp.getPredatorEnergyOutputRate(),lsdp.getPredatorMaintainSpeedTime(), 
						lsdp.getPredatorMaxSpeedHOD(), lsdp.getPredatorMaxSpeedHED(), lsdp.getPredatorMaxSpeedHOR(),
						lsdp.getPredatorGestationPeriod(),lsdp.PredatorEnergy);
				
				predatorList.add(p);
				Platform.runLater(()-> {
					SimView.realizePredator(p.GetXPos(), p.GetYPos());
				});
				
			}
			else
			{
				System.err.println("Failed to read data for predator " + i);
			}
			
	
		}
		for(int i=0; i< iObstacleCount; i++)
		{
			if(lsdp.getObstacleData())
			{
				Obstacle o = new Obstacle(lsdp.ObstacleX, lsdp.ObstacleY,lsdp.ObstacleDiameter, lsdp.ObstacleHeight);
				obstacleList.add(o);
				Platform.runLater(()-> {
					SimView.realizeObstacle(o.GetXPos(), o.GetYPos(), (int)o.getDiameter());
				});
			}
			else
			{
				System.err.println("Failed to read data for obstacle " + i);
			}
			
		}
		
	}
	
	public void run()
	{
		
		// Determine if one of the lifeforms has reached zero, and stop simulation if so
		if(plantList.size() == 0)
		{
			Platform.runLater(()-> {
				SimView.displayEOSMessage("Plant");
			});
			Driver.stopSim();
		} else if(grazerList.size() == 0)
		{
			Platform.runLater(()-> {
				SimView.displayEOSMessage("Grazer");
			});
			Driver.stopSim();
		} else if (predatorList.size() == 0)
		{
			Platform.runLater(()-> {
				SimView.displayEOSMessage("Predator");
			});
			Driver.stopSim();
		}	
		
		// Clear screen of previous lifeform graphics
		Platform.runLater(()-> {
			SimView.clearLifeForms();
		});
		
		for(int x=0; x<plantList.size();x++)
		{
			Plant thisPlant = plantList.get(x);
			//ArrayList<Plant> templist=plantList.get(x).Run();
			//plantList.addAll(templist);
			
			Platform.runLater(()-> {
				ArrayList<Plant> templist=thisPlant.Run();
				plantList.addAll(templist);
				SimView.realizePlant(thisPlant.GetXPos(), thisPlant.GetYPos(), thisPlant.getDiameter());
			}); 
				
			

		} 
		for(int x=0; x<obstacleList.size();x++)
		{
			Obstacle thisObstacle = obstacleList.get(x);
			
			Platform.runLater(()-> {
				//ArrayList<Obstacle> templist=thisObstacle.Run();
				//obstacleList.addAll(templist);
				SimView.realizeObstacle(thisObstacle.GetXPos(), thisObstacle.GetYPos(), (int)thisObstacle.getDiameter());
			}); 
				
			

		}
		/*
		for(Predator i : predatorList)
=======
			Plant thisPlant = plantList.get(x);
			thisPlant.Run();
			//Platform.runLater(()-> {
				//SimView.realizePlant(thisPlant.GetXPos(), thisPlant.GetYPos(), thisPlant.getDiameter());
			//});
		}
		*/
		for (int x = 0; x<predatorList.size(); x++) {
			Predator thisPredator = predatorList.get(x);
			Platform.runLater(()-> {
				boolean rep = thisPredator.Run();
				SimView.realizePredator(thisPredator.GetXPos(), thisPredator.GetYPos());
				if(rep) {
					//ArrayList<Predator> newCubs = new ArrayList<Predator>();
					int numCubs = (int)Math.round(Math.random()*lsdp.getPredatorMaxOffspring()+1);
					for(int i=0; i<numCubs; i++) {
						String[] thisMate = thisPredator.getMate();
						int firstGene = (int) Math.round(Math.random());
						String newStrength = thisPredator.getStrength().substring(firstGene,firstGene+1)+thisMate[0].charAt((int)(Math.round(Math.random())));
						if(newStrength.equals("sS")) {
							newStrength = "Ss";
						}
						firstGene = (int) Math.round(Math.random());
						String newAggression = thisPredator.getAggression().substring(firstGene,firstGene+1)+thisMate[1].charAt((int)(Math.round(Math.random())));
						if(newAggression.equals("aA")) {
							newAggression = "Aa";
						}
						firstGene = (int) Math.round(Math.random());
						String newSpeed = thisPredator.getSpeed().substring(firstGene,firstGene+1)+thisMate[2].charAt((int)(Math.round(Math.random())));
						if(newSpeed.equals("fF")) {
							newSpeed = "Ff";
						}
						predatorList.add(new Predator(thisPredator.GetXPos(),thisPredator.GetYPos(),newAggression,newStrength,newSpeed,lsdp.getPredatorEnergyToReproduce(), 
								lsdp.getPredatorEnergyOutputRate(),lsdp.getPredatorMaintainSpeedTime(), lsdp.getPredatorMaxSpeedHOD(), lsdp.getPredatorMaxSpeedHED(), lsdp.getPredatorMaxSpeedHOR(),lsdp.getPredatorGestationPeriod(),lsdp.getPredatorOffspringEnergyLevel()));
						predatorList.get(predatorList.size()-1).setBirthTime();
					}
				}
			});
		}
		for(int x=0; x<grazerList.size();x++)
		{
		
			Grazer thisGrazer = grazerList.get(x);
			Platform.runLater(()-> {
				ArrayList<Grazer> templist=thisGrazer.Run();
				grazerList.addAll(templist);
				SimView.realizeGrazer(thisGrazer.GetXPos(), thisGrazer.GetYPos());
			});
			if (thisGrazer.getDeleted()==1)
			{
				grazerList.remove(x);
				
			}
		}

	}
	public ArrayList<Plant> getPlants()
	{
		return plantList;
	}
	public ArrayList<Predator> getPredators()
	{
		return predatorList;
	}
	public ArrayList<Grazer> getGrazers()
	{
		return grazerList;
	}
	public ArrayList<Obstacle> getObstacles()
	{
		return obstacleList;
	}
	public void addToGrazers(Grazer g)
	{
		grazerList.add(g);
	}
	
	public void setFile(File file)
	{
		if(file != null)
		{
			dataFile = file;
			
			loadNewRun();
			
		} else
		{
			System.err.println("ERROR: File passed in does not exist!");
		}
	}

	public double getMaxX() {
		return maxX;
		
	}
	public double getMaxY() {
		return maxY;
		
	}
	public double getPlantGrowthRate() {
		return plantGrowthRate;
	}
	
	/**
	 * Creates a report at the instance that the create report
	 * button was pressed. Report contains the number and state
	 * of all plants, grazers, and predators and is labeled
	 * SimReport_hh_mm_ss.txt where hh, mm, and ss are hours,
	 * minutes, and seconds of the system time.
	 */
	public void createReport()
	{
		
		// Get the current date/time
    	Date currentTime = new Date(System.currentTimeMillis());
    	// Convert to hh_mm_ss (hours, minutes, seconds)
    	String fileTag = sdf1.format(currentTime);
    	
    	// Append simple time format
    	String fileName = "SimReport_" + fileTag + ".txt";

    	// Get path to downloads folder
    	String home = System.getProperty("user.home");
    	
    	// Create the file in the downloads folder
    	File reportFile = new File(home + "/Downloads/" + fileName);
    	    	
    	try
    	{
    		FileWriter writer = new FileWriter(reportFile);
    	
    		writer.write("Life Simulator Report\n");
    		writer.write("Created at time: " + fileTag + "\n\n");
    		
    		writer.write("Number of plants:" + plantList.size() + "\n");
    		writer.write("Number of grazers:" + grazerList.size() + "\n");
    		writer.write("Number of predators:" + predatorList.size() + "\n\n");
    		
    		
    		for(Plant p: plantList)
    		{
    			writer.write("Plant: " + (plantList.lastIndexOf(p) + 1) + "\n");
    			writer.write("Size (Diameter): " + p.getDiameter() + "\n\n");
    		}
    		
    		for(Grazer g: grazerList)
    		{
    			writer.write("Grazer: " + (grazerList.lastIndexOf(g) + 1) + "\n");
    			writer.write("Energy : " + g.getEnergy() + "\n\n");
    		}
    		
    		for(Predator p: predatorList)
    		{
    			writer.write("Predator: " + (predatorList.lastIndexOf(p) + 1) + "\n");
    			writer.write("Energy : " + p.getEnergy() + "\n\n");
    		}

    		writer.close();
    	} catch (Exception e)
    	{
    		System.err.println("ERROR: Unable to write to report file!");
    	}

	}

}
