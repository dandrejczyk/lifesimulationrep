
/**
 * @author Daniel Andrejczyk
 *
 * This class extends Thread and makes calls to GameObjectManager
 * to update different application parts each frame such as the
 * overview in the GUI.
 */
public class UpdateThread extends Thread {
	
	// Delay in milliseconds.
	private int delay;
	private boolean running;
	private long lastSysTime;
	GameObjectManager updateObj= GameObjectManager.getInstance();
	
	/**
	 * Creates a new instance of this Thread class.
	 * 
	 * @param delayMillis	The delay in milliseconds.
	 */
	public UpdateThread(int delayMillis)
	{
		delay = delayMillis;
		running = true;
		
	}
	
	/**
	 * Shuts down this thread after it has
	 * completed its last wait interval.
	 */
	public void shutDown()
	{
		running = false;
		
	}
	
	/**
	 * Updates the amount of time the thread
	 * will sleep before updating the application frame.
	 * 
	 * @param delayMillis	Amount of time to sleep in milliseconds.
	 */
	public void updateDelay(int delayMillis)
	{
		if(delayMillis > 0)
		{
			delay = delayMillis;
		} else
		{
			System.err.println("The updated delay must be a positive integer in milliseconds!");
		}
	}
	
	/**
	 * Defines thread behavior. This thread will make
	 * calls each update to GameObjectManager to
	 * update the overview in GUI and other parts of
	 * the application.
	 */
	@Override
	public void run()
	{
		while(running)
		{
			lastSysTime = System.currentTimeMillis();
			updateObj.getInstance().run();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				System.err.println("Update thread was interrupted!");
			}
		}
	}

}
